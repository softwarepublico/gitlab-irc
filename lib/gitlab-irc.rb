require 'rubygems'
require 'sinatra'
require 'json'
require 'yaml'
require 'cinch'

$config = YAML.load(File.read('./config/config.yml'))
$bot = Cinch::Bot.new do
  configure do |c|
    c.server = $config['IRC_HOST']
    c.channels = $config['IRC_CHANNELS']
    c.port = $config['IRC_PORT']
    if $config['SSL'] == true
        c.ssl.use = true
    end
    c.nick = $config['IRC_NICK']
    c.user = $config['IRC_NICK']
    c.realname = $config['IRC_REALNAME']
    c.verbose = $config['DEBUG']
  end

  on :message, "hello" do |m|
    m.reply "Hello, #{m.user.nick}"
  end
end

Thread.new do
    $bot.start
end

def say(msg)
    puts "All channels: #{$bot.channels}"
    $bot.channels.each do |channel|
        puts "Sending notice to channel"
        channel.send msg
    end
end

post '/commit' do
    json = JSON.parse(request.body.read)
    json['commits'].each do |commit|
        commit_message = commit['message'].gsub(/\n/," ")
        say "[#{json['repository']['name'].capitalize}] #{commit['author']['name']} | #{commit_message}"
        say "           View Commit: #{commit['url']}"
    end
    "OK"
end
